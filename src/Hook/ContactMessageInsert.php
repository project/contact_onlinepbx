<?php

namespace Drupal\contact_onlinepbx\Hook;

use Drupal\contact\MessageInterface;
use Drupal\contact_onlinepbx\Controller\Api;

/**
 * Implemets hook_contact_message_insert.
 */
class ContactMessageInsert {

  /**
   * Hook.
   */
  public static function hook(MessageInterface $contact_message) {
    $contact_form_id = $contact_message->getContactForm()->id();
    if (self::contactFormWithoutPbxCall($contact_message)) {
      return;
    }
    foreach (self::getFields() as $field) {
      if ($contact_message->hasField($field) && $contact_message->$field->value) {
        if ($phone = self::tryPhone($contact_message->$field->value)) {
          Api::callNow($phone);
        }
      }
    }
  }

  /**
   * Find phone in string.
   */
  private static function tryPhone($str) {
    $phone = FALSE;
    $numbers = preg_replace("/[^0-9]/", '', $str);
    $stip = substr($numbers, 1, 10);
    if (strlen($stip) == 10) {
      $phone = "8" . $stip;
    }
    return $phone;
  }

  /**
   * Get Fields From Settings.
   */
  private static function getFields() {
    $config = \Drupal::config('contact_onlinepbx.settings');
    $fields = $config->get('fields');
    $result = [];
    foreach (explode("\n", $fields) as $line) {
      if (trim($line)) {
        $result[] = trim($line);
      }
    }
    return $result;
  }

  /**
   * No need to call.
   */
  private static function contactFormWithoutPbxCall(MessageInterface $contact_message) : bool {
    $contact_forms = self::getContactForms();
    $contact_form_id = $contact_message->getContactForm()->id();
    return !in_array($contact_form_id, $contact_forms);
  }

  /**
   * Get contact forms From Settings.
   */
  private static function getContactForms() : array {
    $config = \Drupal::config('contact_onlinepbx.settings');
    $contact_forms = $config->get('contact_forms');
    $result = [];
    foreach (explode("\n", $contact_forms) as $line) {
      if (trim($line)) {
        $result[] = trim($line);
      }
    }
    return $result;
  }

}
